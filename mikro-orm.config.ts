import { MikroORM } from '@mikro-orm/core';

export default {
  entities: ['dist/**/*.entity.js'],
  entitiesTs: ['src/todo/**.entity.ts'],
  dbName: 'postgres', 
  user: 'postgres',
  password: '0810', 
  type: 'postgresql',
  debug: process.env.NODE_ENV !== 'production',
} as Parameters<typeof MikroORM.init>[0];

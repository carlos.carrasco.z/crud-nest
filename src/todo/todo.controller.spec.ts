import { Test, TestingModule } from '@nestjs/testing';
import { TodoController } from './todo.controller';
import { TodoService } from './todo.service';
import {Todo} from './todo.entity'

describe('TodoController', () => {
  let controller: TodoController;
  let service: TodoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TodoController],
      providers: [
        {
          provide: TodoService,
          useValue: {
            findAll: jest.fn(),
            findOne: jest.fn(),
            create: jest.fn(),
            update: jest.fn(),
            remove: jest.fn()
          }
      }],
    }).compile();

    controller = module.get<TodoController>(TodoController);
    service = module.get<TodoService>(TodoService);
  });


  describe('findAll', () => {
    it('return array', async () => {  
      const expectedTodos: Todo[] = [
        { id: 1, title: 'Todo 1', completed: false},
        { id: 2, title: 'Todo 2', completed: false},
      ];

      service.findAll = jest.fn().mockResolvedValue(expectedTodos);
      const result = await controller.findAll();
      expect(result).toEqual(expectedTodos);
    });
  });

  describe('findOne', () => {
    it('return a user',async () => {
      const id = 1;
      const expectedResult: Todo = {
        id: 1,
        title: "Prueba",
        completed: false
      } as Todo
      service.findOne = jest.fn().mockResolvedValue(expectedResult);
      const result = await controller.findOne(id);
      expect(result).toBe(expectedResult);
    });
  });

  describe('create', () => {
    it('create', async () => {
      const todoData: Partial<Todo> = { title: 'Test Todo', completed: false };
      const expectedResult: Todo = { id: 1, ...todoData } as Todo;
      service.create = jest.fn().mockResolvedValue(expectedResult);

      const result = await controller.create(todoData);
      expect(result).toBe(expectedResult);
    });
  });

  describe('update', () => {
    it('update by id', async () => {
      const id = 1;
      const todoData: Partial<Todo> = { title: 'Updated', completed: true };
      const expectedResult: Todo = { id: id, ...todoData } as Todo;
      service.update = jest.fn().mockResolvedValue(expectedResult);

      const result = await controller.update(id, todoData);
      expect(result).toBe(expectedResult);
    });
  });

  describe('remove', () => {
    it('remove by id', async () => {
      const id = 1;
      service.remove = jest.fn().mockResolvedValue({ success: true });

      const result = await controller.remove(id);
      expect(result).toEqual({ success: true });
    });
  });

});

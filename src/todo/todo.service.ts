import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/core';
import { Todo } from './todo.entity';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todo)
    private readonly todoRepository: EntityRepository<Todo>,
  ) {}

  async create(data: Partial<Todo>): Promise<Todo> {
    const todo = this.todoRepository.create(data);
    await this.todoRepository.persistAndFlush(todo);
    return todo;
  }

  async findAll(): Promise<Todo[]> {
    return this.todoRepository.findAll();
  }

  async findOne(id: number): Promise<Todo | null> {
    return this.todoRepository.findOne(id);
  }

  async update(id: number, data: Partial<Todo>): Promise<Todo | null> {
    const todo = await this.todoRepository.findOne(id);
    if (!todo) return null;

    Object.assign(todo, data);
    await this.todoRepository.persistAndFlush(todo);
    return todo;
  }

  async remove(id: number): Promise<boolean> {
    const todo = await this.todoRepository.findOne(id);
    if (!todo) return false;

    await this.todoRepository.removeAndFlush(todo);
    return true;
  }
}

import { Controller, Get, Post, Put, Delete, Body, Param } from '@nestjs/common';
import { TodoService } from './todo.service';
import { Todo } from './todo.entity';

@Controller('todos')
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Post()
  async create(@Body() todoData: Partial<Todo>): Promise<Todo> {
    return this.todoService.create(todoData);
  }

  @Get()
  async findAll(): Promise<Todo[]> {
    return this.todoService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<Todo | null> {
    return this.todoService.findOne(id);
  }

  @Put(':id')
  async update(@Param('id') id: number, @Body() todoData: Partial<Todo>): Promise<Todo | null> {
    return this.todoService.update(id, todoData);
  }

  @Delete(':id')
  async remove(@Param('id') id: number): Promise<boolean> {
    const success = await this.todoService.remove(id);
    return success;
  }
}

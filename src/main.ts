import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
//import { MikroOrmModule } from '@mikro-orm/nestjs';
//import mikroOrmConfig from '../mikro-orm.config';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(3000);
}

bootstrap();
